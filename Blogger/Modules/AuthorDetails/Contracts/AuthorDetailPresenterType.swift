import AdvancedList
import SwiftUI

protocol AuthorDetailPresenterType: class {
    associatedtype PaginationErrorView: View
    associatedtype PaginationLoadingView: View
    
    var dependencies: AuthorDetailPresenterDependenciesType { get set }

    var listService: ListService { get }
    var pagination: AdvancedListPagination<PaginationErrorView, PaginationLoadingView> { get }
    
    func didReceiveEvent(_ event: AuthorDetailEvent)
    func didTriggerAction(_ action: AuthorDetailAction)
}
