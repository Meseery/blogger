import Foundation

protocol AuthorDetailInteractorDependenciesType {
    var remoteDataProvider: DataServiceType { get }
    var authorId: Int { get set }
}
