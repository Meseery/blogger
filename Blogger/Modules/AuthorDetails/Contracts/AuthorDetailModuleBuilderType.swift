import Foundation

protocol AuthorDetailModuleBuilderType {
    static func build(author: Author) -> AuthorDetailPresenter
}
