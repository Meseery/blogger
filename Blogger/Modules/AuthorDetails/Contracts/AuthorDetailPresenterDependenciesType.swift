import Foundation

protocol AuthorDetailPresenterDependenciesType {
    var interactor: AuthorDetailInteractorType { get set }
    var author: Author { get set }
}
