import Combine
import Foundation

protocol AuthorDetailInteractorType {
    var dependencies: AuthorDetailInteractorDependenciesType { get set }
    var allPostsLoaded: Bool { get set }

    func getAuthorPosts() -> AnyPublisher<[Post], Error>
    func getNextAuthorPosts() -> AnyPublisher<[Post], Error>
}
