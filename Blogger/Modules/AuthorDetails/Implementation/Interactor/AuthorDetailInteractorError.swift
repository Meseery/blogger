import Foundation

enum AuthorDetailInteractorError: Error {
    case allPostsLoaded
}
