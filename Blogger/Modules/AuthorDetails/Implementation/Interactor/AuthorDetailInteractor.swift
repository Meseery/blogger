import Combine
import Foundation

final class AuthorDetailInteractor {
    var dependencies: AuthorDetailInteractorDependenciesType
    var allPostsLoaded: Bool = false

    private var page = 0
    private var limit = 30

    init(dependencies: AuthorDetailInteractorDependenciesType) {
        self.dependencies = dependencies
    }
}

extension AuthorDetailInteractor: AuthorDetailInteractorType {
    
    func getAuthorPosts() -> AnyPublisher<[Post], Error> {
        return dependencies
                .remoteDataProvider
                .getPosts(authorId: dependencies.authorId,
                          page: page,
                          limit: limit)
    }
    
    func getNextAuthorPosts() -> AnyPublisher<[Post], Error> {
        guard !allPostsLoaded else {
            return Fail(outputType: [Post].self,
                        failure: AuthorDetailInteractorError.allPostsLoaded).eraseToAnyPublisher()
        }
        page += 1
        return dependencies
            .remoteDataProvider
            .getPosts(authorId: dependencies.authorId,
                      page: page,
                      limit: limit)
    }
}
