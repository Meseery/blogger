import AdvancedList
import SwiftUI

struct AuthorDetailView: View {
    @ObservedObject private var presenter: AuthorDetailPresenter
    
    var body: some View {
            VStack {
                Text(presenter.authorName)
                    .font(.largeTitle)
                    .foregroundColor(Color.green)
                Text(presenter.authorUsername)
                    .font(.headline)
                    .foregroundColor(Color.yellow)
                AdvancedList(listService: presenter.listService,
                             emptyStateView: {
                                Text("No Posts Found!")
                }, errorStateView: { error in
                    VStack {
                        Text(error.localizedDescription)
                            .lineLimit(nil)
                            .multilineTextAlignment(.center)
                        
                        Button(action: {
                            self.presenter.didTriggerAction(.retry)
                        }) {
                            Text("Retry")
                        }.padding()
                    }
                }, loadingStateView: {
                    Text("Loading...")
                }, pagination: presenter.pagination)
            }
            .onAppear { self.presenter.didReceiveEvent(.viewAppears) }
            .onDisappear { self.presenter.didReceiveEvent(.viewDisappears) }
    }
    init(author: Author) {
        self.presenter = AuthorDetailModuleBuilder.build(author: author)
    }
}


#if DEBUG
struct AuthorDetailView_Previews: PreviewProvider {
    static var previews: some View {
        let author = Author(id: 0,
                            name: "Paulo Cohello",
                            userName: "@PCohello",
                            email: "paulo@gmail.com",
                            avatarUrl: "")
        return AuthorDetailView(author: author)
    }
}
#endif
