import SwiftUI
import Foundation

struct PostRowView {
    private let post: Post
    
    init(_ post: Post) {
        self.post = post
    }
}

extension PostRowView: Identifiable {
    var id: ObjectIdentifier {
        post.id
    }
}

extension PostRowView: View {
    var body: some View {
        NavigationLink(destination: PostDetailView(post: post)){
            VStack(alignment: .leading) {
                Text(post.title)
                    .font(.headline)
                    .fixedSize(horizontal: false, vertical: true)
                Text(DateFormatter.PresentationDateFormatter.string(from: post.postDate))
                    .font(.caption)
                Divider().background(Color.green).frame(width: 120, height: 5)
                Text(post.body)
                    .fixedSize(horizontal: false, vertical: true)
            }.padding(.bottom, 10)            
        }
    }
}


