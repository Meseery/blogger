import Foundation

enum AuthorDetailEvent {
    case viewAppears
    case viewDisappears
}
