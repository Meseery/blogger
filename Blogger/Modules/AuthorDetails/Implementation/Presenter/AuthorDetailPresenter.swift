import AdvancedList
import SwiftUI
import Combine

final class AuthorDetailPresenter: ObservableObject {
    var dependencies: AuthorDetailPresenterDependenciesType
    
    var authorName: String {
        return dependencies.author.name
    }
    
    var authorUsername: String {
        return dependencies.author.userName
    }
    
    var authorEmail: String {
        return dependencies.author.email
    }
    
    private var getCurrentAuthorPostsCancellable: AnyCancellable?
    private var getNextAuthorPostsCancellable: AnyCancellable?
    
    let listService = ListService()
    
    private(set) lazy var pagination: AdvancedListPagination<AnyView, AnyView> = {
        .thresholdItemPagination(errorView: { error in
            AnyView(
                VStack {
                    Text(error.localizedDescription)
                        .lineLimit(nil)
                        .multilineTextAlignment(.center)
                    
                    Button(action: {
                        self.getAuthorPosts(initialLoading: false)
                    }) {
                        Text("Retry")
                    }.padding()
                }
            )
        }, loadingView: {
            AnyView(
                VStack {
                    Divider()
                    Text("Fetching more posts...")
                }
            )
        }, offset: 10, shouldLoadNextPage: {
            self.getNextAuthorPosts()
        }, state: .idle)
    }()

    init(dependencies: AuthorDetailPresenterDependenciesType) {
        self.dependencies = dependencies
    }
}

extension AuthorDetailPresenter: AuthorDetailPresenterType {
    func didReceiveEvent(_ event: AuthorDetailEvent) {
        switch event {
            case .viewAppears:
                getAuthorPosts(initialLoading: true)
            case .viewDisappears:
                getCurrentAuthorPostsCancellable?.cancel()
                getNextAuthorPostsCancellable?.cancel()
                listService.listState = .items
                pagination.state = .idle
        }
    }

    func didTriggerAction(_ action: AuthorDetailAction) {
        switch action {
            case .retry:
                getAuthorPosts(initialLoading: true)
        }
    }
}

extension AuthorDetailPresenter {
    private func getAuthorPosts(initialLoading: Bool) {
        if initialLoading {
            listService.listState = .loading
        } else {
            pagination.state = .loading
        }
        
        getCurrentAuthorPostsCancellable = dependencies.interactor
            .getAuthorPosts()
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { (completion) in
                switch completion {
                    case let .failure(error):
                        if initialLoading {
                            self.listService.listState = .error(error)
                        } else {
                            self.pagination.state = .error(error)
                    }
                    case .finished:
                        if initialLoading {
                            self.listService.listState = .items
                        } else {
                            self.pagination.state = .idle
                    }
                }
            }, receiveValue: { (posts) in
                let postsRows = posts.map (PostRowView.init)
                self.listService.appendItems(postsRows)
            })
    }
    
    private func getNextAuthorPosts() {
        guard !dependencies.interactor.allPostsLoaded, pagination.state == .idle else {
            return
        }
        pagination.state = .loading
        
        getNextAuthorPostsCancellable = dependencies.interactor.getNextAuthorPosts()
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                    case .failure(let error):
                        if let interactorError = error as? AuthorDetailInteractorError, interactorError == .allPostsLoaded {
                            self.pagination.state = .idle
                        } else {
                            self.pagination.state = .error(error)
                    }
                    case .finished:
                        self.pagination.state = .idle
                }
            }, receiveValue: { posts in
                let postsRows = posts.map (PostRowView.init)
                self.listService.appendItems(postsRows)
            })
    }
}
