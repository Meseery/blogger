import Foundation


struct AuthorDetailModuleBuilder: AuthorDetailModuleBuilderType {
    static func build(author: Author) -> AuthorDetailPresenter {
        let interactorDependencies = AuthorDetailInteractorDependencies(authorId: 0)
        let interactor = AuthorDetailInteractor(dependencies: interactorDependencies)

        let presenterDependencies = AuthorDetailPresenterDependencies(interactor: interactor, author: author)
        let presenter = AuthorDetailPresenter(dependencies: presenterDependencies)
        return presenter
    }
}
