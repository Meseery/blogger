import Foundation

struct AuthorDetailPresenterDependencies: AuthorDetailPresenterDependenciesType {
    var interactor: AuthorDetailInteractorType
    var author: Author
    
    init(interactor: AuthorDetailInteractorType, author: Author) {
        self.interactor = interactor
        self.author = author
    }
}
