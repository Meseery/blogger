import Foundation

struct AuthorDetailInteractorDependencies: AuthorDetailInteractorDependenciesType {
    var remoteDataProvider: DataServiceType
    var authorId: Int
    
    init(remoteDataProvider: DataServiceType = RemoteDataService(), authorId: Int) {
        self.remoteDataProvider = remoteDataProvider
        self.authorId = authorId
    }
}
