import Foundation

protocol AuthorsInteractorDependenciesType {
    var remoteDataProvider: DataServiceType { get }
}
