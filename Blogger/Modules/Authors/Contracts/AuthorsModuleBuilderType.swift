import Foundation

protocol AuthorsModuleBuilderType {
    static func build() -> AuthorsPresenter
}
