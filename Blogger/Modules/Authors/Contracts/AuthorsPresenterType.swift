import AdvancedList
import SwiftUI

protocol AuthorsPresenterType: class {
    associatedtype PaginationErrorView: View
    associatedtype PaginationLoadingView: View
    
    var dependencies: AuthorsPresenterDependenciesType { get set }

    var listService: ListService { get }
    var pagination: AdvancedListPagination<PaginationErrorView, PaginationLoadingView> { get }
    
    func didReceiveEvent(_ event: AuthorsEvent)
    func didTriggerAction(_ action: AuthorsAction)
}
