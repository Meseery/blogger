import Combine
import Foundation

protocol AuthorsInteractorType {
    var dependencies: AuthorsInteractorDependenciesType { get set }
    var allAuthorsLoaded: Bool { get set }
    func getAuthors() -> AnyPublisher<[Author], Error>
    func getNextAuthors() -> AnyPublisher<[Author], Error>
}
