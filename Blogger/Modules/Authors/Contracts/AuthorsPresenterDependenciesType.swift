import Foundation

protocol AuthorsPresenterDependenciesType {
    var interactor: AuthorsInteractorType { get set }
}
