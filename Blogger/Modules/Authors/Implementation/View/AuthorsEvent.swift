import Foundation

enum AuthorsEvent {
    case viewAppears
    case viewDisappears
}
