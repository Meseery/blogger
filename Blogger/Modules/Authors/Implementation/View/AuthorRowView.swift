import SwiftUI
import Foundation

struct AuthorRowView {
    private let author: Author
    
    init(_ author:Author) {
        self.author = author
    }
}

extension AuthorRowView: Identifiable {
    var id: ObjectIdentifier {
        author.id
    }
}

extension AuthorRowView: View {
    var body: some View {
         NavigationLink(destination: AuthorDetailView(author: author)) {
            VStack(alignment: .leading) {
                Text(author.name)
                    .font(.body)
                Text(author.userName)
                    .font(.caption)
            }
        }
    }
}
