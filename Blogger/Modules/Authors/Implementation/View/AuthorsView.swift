import SwiftUI
import AdvancedList

struct AuthorsView: View {
    @ObservedObject private var presenter: AuthorsPresenter
    
    var body: some View {
        NavigationView {
            #if targetEnvironment(macCatalyst)
            AdvancedList(emptyStateView: {
                Text("No Data Found!")
            }, errorStateView: { error in
                VStack {
                    Text(error.localizedDescription)
                        .lineLimit(nil)
                        .multilineTextAlignment(.center)
                    
                    Button(action: {
                        self.presenter.didTriggerAction(.retry)
                    }) {
                        Text("Retry")
                    }.padding()
                }
            }, loadingStateView: {
                Text("Loading...")
            })
                .environmentObject(presenter.listService)
                .environmentObject(presenter.pagination)
                .navigationBarTitle(Text("Authors"))
            #else
            AdvancedList(listService: presenter.listService, emptyStateView: {
                Text("No Authors Found!")
            }, errorStateView: { error in
                VStack {
                    Text(error.localizedDescription)
                        .lineLimit(nil)
                        .multilineTextAlignment(.center)
                    
                    Button(action: {
                        self.presenter.didTriggerAction(.retry)
                    }) {
                        Text("Retry")
                    }.padding()
                }
            }, loadingStateView: {
                Text("Loading...")
            }, pagination: presenter.pagination)
                .navigationBarTitle(Text("Authors"))
            #endif
        }
        .onAppear {
            self.presenter.didReceiveEvent(.viewAppears)
        }
        .onDisappear {
            self.presenter.didReceiveEvent(.viewDisappears)
        }
    }

    init() {
        self.presenter = AuthorsModuleBuilder.build()
    }
}


#if DEBUG
struct AuthorsView_Previews: PreviewProvider {
    static var previews: some View {
        AuthorsView()
    }
}
#endif
