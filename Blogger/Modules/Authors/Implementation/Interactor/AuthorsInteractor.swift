import Combine

final class AuthorsInteractor {
    internal var dependencies: AuthorsInteractorDependenciesType
    private var page = 0
    private var limit = 30
        
    var allAuthorsLoaded = false
    
    init(dependencies: AuthorsInteractorDependenciesType) {
        self.dependencies = dependencies
    }
}

extension AuthorsInteractor: AuthorsInteractorType {
    func getAuthors() -> AnyPublisher<[Author], Error> {
        dependencies
            .remoteDataProvider
            .getAuthors(page: page, limit: limit)
    }
    
    func getNextAuthors() -> AnyPublisher<[Author], Error> {
        guard !allAuthorsLoaded else {
            return Fail(outputType: [Author].self,
                        failure: AuthorsInteractorError.allAuthorsLoaded).eraseToAnyPublisher()
        }
        page += 1
        return dependencies
            .remoteDataProvider
            .getAuthors(page: page, limit: limit)
    }
}
