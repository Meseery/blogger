import Foundation

enum AuthorsInteractorError: Error {
    case allAuthorsLoaded
}
