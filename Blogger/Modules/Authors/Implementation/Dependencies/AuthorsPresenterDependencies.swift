import Foundation

struct AuthorsPresenterDependencies: AuthorsPresenterDependenciesType {
    var interactor: AuthorsInteractorType
    
    init(interactor: AuthorsInteractorType) {
        self.interactor = interactor
    }
}
