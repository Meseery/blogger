import Foundation

struct AuthorsInteractorDependencies: AuthorsInteractorDependenciesType {
    var remoteDataProvider: DataServiceType
    
    init(remoteDataProvider: DataServiceType = RemoteDataService()) {
        self.remoteDataProvider = remoteDataProvider
    }
}
