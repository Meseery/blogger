import Foundation

struct AuthorsModuleBuilder: AuthorsModuleBuilderType {
    static func build() -> AuthorsPresenter {
        let interactorDependencies = AuthorsInteractorDependencies()
        let interactor = AuthorsInteractor(dependencies: interactorDependencies)

        let presenterDependencies = AuthorsPresenterDependencies(interactor: interactor)
        let presenter = AuthorsPresenter(dependencies: presenterDependencies)
        return presenter
    }
}
