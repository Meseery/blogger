import AdvancedList
import Combine
import SwiftUI

final class AuthorsPresenter: ObservableObject {
    var dependencies: AuthorsPresenterDependenciesType
    
    private var getCurrentAuthorsCancellable: AnyCancellable?
    private var getNextAuthorsCancellable: AnyCancellable?

    let listService = ListService()
    
    private(set) lazy var pagination: AdvancedListPagination<AnyView, AnyView> = {
        .thresholdItemPagination(errorView: { error in
            AnyView(
                VStack {
                    Text(error.localizedDescription)
                        .lineLimit(nil)
                        .multilineTextAlignment(.center)
                    
                    Button(action: {
                        self.getAuthors(initialLoading: false)
                    }) {
                        Text("Retry")
                    }.padding()
                }
            )
        }, loadingView: {
            AnyView(
                VStack {
                    Divider()
                    Text("Fetching more authors...")
                }
            )
        }, offset: 10, shouldLoadNextPage: {
            self.getNextAuthors()
        }, state: .idle)
    }()
    
    init(dependencies: AuthorsPresenterDependenciesType) {
        self.dependencies = dependencies
    }
}

extension AuthorsPresenter: AuthorsPresenterType {
    func didReceiveEvent(_ event: AuthorsEvent) {
        switch event {
            case .viewAppears:
                getAuthors(initialLoading: true)
            case .viewDisappears:
                getCurrentAuthorsCancellable?.cancel()
                getNextAuthorsCancellable?.cancel()
                listService.listState = .items
                pagination.state = .idle
        }
    }

    func didTriggerAction(_ action: AuthorsAction) {
        switch action {
            case .retry:
                getAuthors(initialLoading: true)
        }
    }
}

extension AuthorsPresenter {
    private func getAuthors(initialLoading: Bool) {
        if initialLoading {
            listService.listState = .loading
        } else {
            pagination.state = .loading
        }
        
        getCurrentAuthorsCancellable = dependencies.interactor
            .getAuthors()
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { (completion) in
                switch completion {
                    case let .failure(error):
                        if initialLoading {
                            self.listService.listState = .error(error)
                        } else {
                            self.pagination.state = .error(error)
                        }
                    case .finished:
                        if initialLoading {
                            self.listService.listState = .items
                        } else {
                            self.pagination.state = .idle
                        }
                }
            }, receiveValue: { (authors) in
                let authorsRows = authors.map (AuthorRowView.init)
                self.listService.appendItems(authorsRows)
            })
    }
    
    private func getNextAuthors() {
        guard !dependencies.interactor.allAuthorsLoaded, pagination.state == .idle else {
            return
        }
        pagination.state = .loading
        
        getNextAuthorsCancellable = dependencies.interactor.getNextAuthors()
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                    case .failure(let error):
                        if let interactorError = error as? AuthorsInteractorError, interactorError == .allAuthorsLoaded {
                            self.pagination.state = .idle
                        } else {
                            self.pagination.state = .error(error)
                    }
                    case .finished:
                        self.pagination.state = .idle
                }
            }, receiveValue: { authors in
                let authorsRows = authors.map(AuthorRowView.init)
                self.listService.appendItems(authorsRows)
            })
    }
}
