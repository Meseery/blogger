import Foundation

protocol PostDetailPresenterDependenciesType {
    var interactor: PostDetailInteractorType { get set }
    var post: Post { get set }
}
