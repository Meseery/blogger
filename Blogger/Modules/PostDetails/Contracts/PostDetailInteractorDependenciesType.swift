import Foundation

protocol PostDetailInteractorDependenciesType {
    var remoteDataProvider: DataServiceType { get }
    var postId: Int { get set }
}
