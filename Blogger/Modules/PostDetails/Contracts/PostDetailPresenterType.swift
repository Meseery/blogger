import AdvancedList
import SwiftUI

protocol PostDetailPresenterType: class {
    associatedtype PaginationErrorView: View
    associatedtype PaginationLoadingView: View
    
    var dependencies: PostDetailPresenterDependenciesType { get set }

    var listService: ListService { get }
    var pagination: AdvancedListPagination<PaginationErrorView, PaginationLoadingView> { get }
    
    func didReceiveEvent(_ event: PostDetailEvent)
    func didTriggerAction(_ action: PostDetailAction)
}
