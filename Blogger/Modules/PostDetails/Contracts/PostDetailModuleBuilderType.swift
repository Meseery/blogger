import Foundation

protocol PostDetailModuleBuilderType {
    static func build(post: Post) -> PostDetailPresenter
}
