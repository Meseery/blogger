import Combine
import Foundation

protocol PostDetailInteractorType {
    var dependencies: PostDetailInteractorDependenciesType { get set }
    var allCommentsLoaded: Bool { get set }

    func getPostComments() -> AnyPublisher<[Comment], Error>
    func getNextPostComments() -> AnyPublisher<[Comment], Error>
}
