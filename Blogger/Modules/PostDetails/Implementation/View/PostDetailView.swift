import AdvancedList
import SwiftUI

struct PostDetailView: View {
    @ObservedObject private var presenter: PostDetailPresenter
    
    var body: some View {
            VStack {
                Text(presenter.postTitle)
                    .font(.largeTitle)
                    .foregroundColor(Color.green)
                    .fixedSize(horizontal: false, vertical: true)
                    .multilineTextAlignment(.center)
                Text(presenter.postDate)
                    .font(.headline)
                    .foregroundColor(Color.yellow)
                Text(presenter.postBody)
                    .font(.body)
                    .foregroundColor(Color.black)
                    .fixedSize(horizontal: false, vertical: true)
                    .multilineTextAlignment(.center)
                Divider().background(Color.green).frame(width: 250, height: 5)
                AdvancedList(listService: presenter.listService,
                             emptyStateView: {
                                Text("No Comments Found!")
                }, errorStateView: { error in
                    VStack {
                        Text(error.localizedDescription)
                            .lineLimit(nil)
                            .multilineTextAlignment(.center)
                        
                        Button(action: {
                            self.presenter.didTriggerAction(.retry)
                        }) {
                            Text("Retry")
                        }.padding()
                    }
                }, loadingStateView: {
                    Text("Loading...")
                }, pagination: presenter.pagination)
            }
                .onAppear { self.presenter.didReceiveEvent(.viewAppears) }
                .onDisappear { self.presenter.didReceiveEvent(.viewDisappears) }
    }

    init(post: Post) {
        self.presenter = PostDetailModuleBuilder.build(post: post)
    }
}


#if DEBUG
struct PostDetailView_Previews: PreviewProvider {
    static var previews: some View {
        let post = Post(id: 0, date: "", title: "Sample Post", body: "This's a dummy post", imageUrl: "", authorId: 0)
        return PostDetailView(post: post)
    }
}
#endif
