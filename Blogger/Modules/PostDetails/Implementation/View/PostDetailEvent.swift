import Foundation

enum PostDetailEvent {
    case viewAppears
    case viewDisappears
}
