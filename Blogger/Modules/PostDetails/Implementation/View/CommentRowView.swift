import SwiftUI
import Foundation

struct CommentRowView {
    private let comment: Comment
    
    init(_ comment: Comment) {
        self.comment = comment
    }
}

extension CommentRowView: Identifiable {
    var id: ObjectIdentifier {
        comment.id
    }
}

extension CommentRowView: View {
    var body: some View {
        VStack(alignment: .leading) {
            Text(comment.userName)
                .font(.headline)
                .fixedSize(horizontal: false, vertical: true)
            Text(DateFormatter.PresentationDateFormatter.string(from: comment.commentDate))
                .font(.caption)
            Divider().background(Color.green).frame(width: 120, height: 5)
            Text(comment.body)
                .fixedSize(horizontal: false, vertical: true)
        }.padding(.bottom, 10)
    }
}


