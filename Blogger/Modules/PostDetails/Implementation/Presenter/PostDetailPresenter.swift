import AdvancedList
import SwiftUI
import Combine

final class PostDetailPresenter: ObservableObject {
    var dependencies: PostDetailPresenterDependenciesType
    
    var postTitle: String {
        return dependencies.post.title
    }
    
    var postBody: String {
        return dependencies.post.body
    }
    
    var postDate: String {
        return DateFormatter.PresentationDateFormatter.string(from: dependencies.post.postDate)
    }
    
    private var getCurrentPostCommentsPostsCancellable: AnyCancellable?
    private var getNextPostCommentsCancellable: AnyCancellable?
    
    let listService = ListService()
    
    private(set) lazy var pagination: AdvancedListPagination<AnyView, AnyView> = {
        .thresholdItemPagination(errorView: { error in
            AnyView(
                VStack {
                    Text(error.localizedDescription)
                        .lineLimit(nil)
                        .multilineTextAlignment(.center)
                    
                    Button(action: {
                        self.getPostComments(initialLoading: false)
                    }) {
                        Text("Retry")
                    }.padding()
                }
            )
        }, loadingView: {
            AnyView(
                VStack {
                    Divider()
                    Text("Fetching more posts...")
                }
            )
        }, offset: 10, shouldLoadNextPage: {
            self.getNextPostComments()
        }, state: .idle)
    }()

    init(dependencies: PostDetailPresenterDependenciesType) {
        self.dependencies = dependencies
    }
}

extension PostDetailPresenter: PostDetailPresenterType {
    func didReceiveEvent(_ event: PostDetailEvent) {
        switch event {
            case .viewAppears:
                getPostComments(initialLoading: true)
            case .viewDisappears:
                getCurrentPostCommentsPostsCancellable?.cancel()
                getNextPostCommentsCancellable?.cancel()
                listService.listState = .items
                pagination.state = .idle
        }
    }

    func didTriggerAction(_ action: PostDetailAction) {
        switch action {
            case .retry:
                getPostComments(initialLoading: true)
        }
    }
}

extension PostDetailPresenter {
    private func getPostComments(initialLoading: Bool) {
        if initialLoading {
            listService.listState = .loading
        } else {
            pagination.state = .loading
        }
        
        getCurrentPostCommentsPostsCancellable = dependencies.interactor
            .getPostComments()
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { (completion) in
                switch completion {
                    case let .failure(error):
                        if initialLoading {
                            self.listService.listState = .error(error)
                        } else {
                            self.pagination.state = .error(error)
                    }
                    case .finished:
                        if initialLoading {
                            self.listService.listState = .items
                        } else {
                            self.pagination.state = .idle
                    }
                }
            }, receiveValue: { (comments) in
                
                let commentsRows = comments.map (CommentRowView.init)
                self.listService.appendItems(commentsRows)
            })
    }
    
    private func getNextPostComments() {
        guard !dependencies.interactor.allCommentsLoaded, pagination.state == .idle else {
            return
        }
        pagination.state = .loading
        
        getNextPostCommentsCancellable = dependencies.interactor.getNextPostComments()
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                    case .failure(let error):
                        if let interactorError = error as? PostDetailInteractorError, interactorError == .allCommentsLoaded {
                            self.pagination.state = .idle
                        } else {
                            self.pagination.state = .error(error)
                    }
                    case .finished:
                        self.pagination.state = .idle
                }
            }, receiveValue: { comments in
                let commentsRows = comments.map (CommentRowView.init)
                self.listService.appendItems(commentsRows)
            })
    }
}
