import Foundation

enum PostDetailInteractorError: Error {
    case allCommentsLoaded
}
