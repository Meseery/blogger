import Combine
import Foundation

final class PostDetailInteractor {
    var dependencies: PostDetailInteractorDependenciesType
    var allCommentsLoaded: Bool = false

    private var page = 0
    private var limit = 30

    init(dependencies: PostDetailInteractorDependenciesType) {
        self.dependencies = dependencies
    }
}

extension PostDetailInteractor: PostDetailInteractorType {

    
    func getPostComments() -> AnyPublisher<[Comment], Error> {
        return dependencies
                .remoteDataProvider
                .getComments(postId: dependencies.postId,
                          page: page,
                          limit: limit)
    }
    
    func getNextPostComments() -> AnyPublisher<[Comment], Error> {
        guard !allCommentsLoaded else {
            return Fail(outputType: [Comment].self,
                        failure: PostDetailInteractorError.allCommentsLoaded).eraseToAnyPublisher()
        }
        page += 1
        return dependencies
            .remoteDataProvider
            .getComments(postId: dependencies.postId,
                      page: page,
                      limit: limit)
    }
}
