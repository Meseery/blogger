import Foundation

struct PostDetailModuleBuilder: PostDetailModuleBuilderType {
    static func build(post: Post) -> PostDetailPresenter {
        let interactorDependencies = PostDetailInteractorDependencies(postId: post.id)
        let interactor = PostDetailInteractor(dependencies: interactorDependencies)

        let presenterDependencies = PostDetailPresenterDependencies(interactor: interactor, post: post)
        let presenter = PostDetailPresenter(dependencies: presenterDependencies)
        return presenter
    }
}
