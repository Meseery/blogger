import Foundation

struct PostDetailInteractorDependencies: PostDetailInteractorDependenciesType {
    var remoteDataProvider: DataServiceType
    var postId: Int
    
    init(remoteDataProvider: DataServiceType = RemoteDataService(), postId: Int) {
        self.remoteDataProvider = remoteDataProvider
        self.postId = postId
    }
}
