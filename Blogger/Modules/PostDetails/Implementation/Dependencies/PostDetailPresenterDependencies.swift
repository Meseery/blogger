import Foundation

struct PostDetailPresenterDependencies: PostDetailPresenterDependenciesType {
    var interactor: PostDetailInteractorType
    var post: Post
    
    init(interactor: PostDetailInteractorType, post: Post) {
        self.interactor = interactor
        self.post = post
    }
}
