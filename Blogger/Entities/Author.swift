import SwiftUI

final class Author: Identifiable {
    var id: Int
    var name: String
    var userName: String
    var email: String
    var avatarUrl: String
    
    internal init(id: Int,
                  name: String,
                  userName: String,
                  email: String,
                  avatarUrl: String) {
        self.id = id
        self.name = name
        self.userName = userName
        self.email = email
        self.avatarUrl = avatarUrl
    }
}

extension Author: Decodable {}
