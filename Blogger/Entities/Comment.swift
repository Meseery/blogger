import SwiftUI

final class Comment: Identifiable {
    var id: Int
    var date: String
    var body: String
    var userName: String
    var email: String
    var avatarUrl: String
    var postId: Int
    
    var commentDate: Date {
        return DateFormatter.APIDateFormatter.date(from: date) ?? Date()
    }

    internal init(id: Int,
                  date: String,
                  body: String,
                  userName: String,
                  email: String,
                  avatarUrl: String,
                  postId: Int) {
        self.id = id
        self.date = date
        self.body = body
        self.userName = userName
        self.email = email
        self.avatarUrl = avatarUrl
        self.postId = postId
    }
}

extension Comment: Decodable {}
