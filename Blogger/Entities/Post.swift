import SwiftUI

final class Post: Identifiable {
    var id: Int
    var date: String
    var title: String
    var body: String
    var imageUrl: String
    var authorId: Int
    
    var postDate: Date {
        return DateFormatter.APIDateFormatter.date(from: self.date) ?? Date()
    }
    
    internal init(id: Int,
                  date: String,
                  title: String,
                  body: String,
                  imageUrl: String,
                  authorId: Int) {
        self.id = id
        self.date = date
        self.title = title
        self.body = body
        self.imageUrl = imageUrl
        self.authorId = authorId
    }
}

extension Post: Decodable {}

extension DateFormatter {
    static let APIDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        return formatter
    }()
    
    static let PresentationDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        return formatter
    }()
}
