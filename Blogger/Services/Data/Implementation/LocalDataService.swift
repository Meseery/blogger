import Combine
import Foundation

// TODO: Implement local data store e.g. Realm, CoreData or Cache
class LocalDataService: DataServiceType {
    func getAuthors(page: Int, limit: Int) -> AnyPublisher<[Author], Error> {
        let authors = [Author(id: 0, name: "", userName: "", email: "", avatarUrl: "")]
        return Future<[Author], Error> { promise in
            promise(.success(authors))
        }.eraseToAnyPublisher()
    }
    
    func getPosts(authorId: Int, page: Int, limit: Int) -> AnyPublisher<[Post], Error> {
        let posts = [Post(id: 0, date: "", title: "", body: "", imageUrl: "", authorId: 1)]
        return Future<[Post], Error> { promise in
            promise(.success(posts))
        }.eraseToAnyPublisher()
    }
    
    func getComments(postId: Int, page: Int, limit: Int) -> AnyPublisher<[Comment], Error> {
        let comments = [Comment(id: 0, date: "", body: "", userName: "", email: "", avatarUrl: "", postId: 1)]
        return Future<[Comment], Error> { promise in
            promise(.success(comments))
        }.eraseToAnyPublisher()
    }
}
