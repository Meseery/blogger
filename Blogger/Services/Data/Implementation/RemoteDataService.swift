import Combine
import Foundation

class RemoteDataService: DataServiceType {
    func getAuthors(page: Int,
                    limit: Int) -> AnyPublisher<[Author], Error> {
        return AuthorsEndpoint.authors(page, limit: limit).execute(responseType: [Author].self)
    }
    
    func getPosts(authorId: Int, page: Int,
                  limit: Int) -> AnyPublisher<[Post], Error> {
        return AuthorsEndpoint.posts(authorId: String(authorId), page: page, limit: limit).execute(responseType: [Post].self)
    }
    
    func getComments(postId: Int, page: Int,
                     limit: Int) -> AnyPublisher<[Comment], Error> {
        return AuthorsEndpoint
            .comments(postId: String(postId), page: page, limit: limit)
            .execute(responseType: [Comment].self)
            .map({$0.sorted { $0.commentDate < $1.commentDate }})
            .eraseToAnyPublisher()
    }
}
