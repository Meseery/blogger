import Combine
import Foundation

protocol DataServiceType {
    func getAuthors(page: Int,
                    limit: Int) -> AnyPublisher<[Author], Error>
    func getPosts(authorId:Int,
                  page: Int,
                  limit: Int) -> AnyPublisher<[Post], Error>
    func getComments(postId:Int,
                     page: Int,
                     limit: Int) -> AnyPublisher<[Comment], Error>
}
