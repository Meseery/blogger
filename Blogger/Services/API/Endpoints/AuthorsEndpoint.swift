enum AuthorsEndpoint: APIEndpointType {
    case authors(_ page:Int, limit: Int)
    case posts(authorId: String, page:Int, limit: Int)
    case comments(postId: String, page: Int, limit: Int)
    
    var baseURL: String {
        return "sym-json-server.herokuapp.com"
    }
    
    var path: String? {
        switch self {
            case .authors:
                return "/authors"
            case .posts:
                return "/posts"
            case .comments:
                return "/comments"
        }
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var parameters: Parameters? {
        switch self {
            case let .authors(page, limit):
                return ["_page":page, "_limit":limit]
            case let .posts(authorID, page, limit):
                return ["authorID":authorID, "_page":page, "_limit": limit]
            case let .comments(postId, page, limit):
                return ["postId":postId, "_page":page, "_limit":limit]
        }
    }
    
    var headers: HTTPHeaders? { return nil }
    
    var scheme: String? { return "https"}
}
