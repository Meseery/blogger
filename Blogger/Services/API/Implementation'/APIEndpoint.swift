import Combine
import Foundation

extension APIEndpointType {
    func execute<U: Decodable>(responseType: U.Type,
                               extraHeaders: [String:String]? = nil) -> AnyPublisher<U, Error>  {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = scheme ?? "https"
        urlComponents.host = baseURL
        
        if let path = self.path { urlComponents.path = path }
        
        if let params = parameters, !params.isEmpty {
            urlComponents.queryItems = [URLQueryItem]()
            for (key, value) in params {
                let queryItem = URLQueryItem(name: key, value: "\(value)")
                urlComponents.queryItems!.append(queryItem)
            }
        }
        
        guard let url = urlComponents.url else {
            return Fail(error: APIError.invalidEndpoint).eraseToAnyPublisher()
        }
        
        var requestHeaders = headers
        if let extraHeaders = extraHeaders {
            requestHeaders?.merge(extraHeaders, uniquingKeysWith: {return $1})
        }
        
        var request = URLRequest(url: url)
        request.allHTTPHeaderFields = requestHeaders
        request.httpMethod = method.rawValue
        
        return URLSession.shared.dataTaskPublisher(for: request)
            .map {$0.data}
            .decode(type: U.self, decoder: JSONDecoder())
            .mapError {
                switch $0 {
                    case is DecodingError: return APIError.parsingError
                    case is URLError: return APIError.networkError(($0 as! URLError).errorCode)
                    default: return APIError.networkException
                }
            }
            .eraseToAnyPublisher()
    }
}
