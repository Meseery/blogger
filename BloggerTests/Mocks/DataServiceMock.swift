import Foundation
import Combine
@testable import Blogger

class DataServiceMock: DataServiceType {
    func getAuthors(page: Int, limit: Int) -> AnyPublisher<[Author], Error> {
        let authors = Bundle(for: type(of: self)).decode([Author].self, from: "Authors.json")
        return Future<[Author], Error> { promise in
            guard let author = authors?[page] else {
                promise(.failure(APIError.invalidData))
                return
            }
            promise(.success([author]))
        }.eraseToAnyPublisher()
    }
    
    func getPosts(authorId: Int, page: Int, limit: Int) -> AnyPublisher<[Post], Error> {
        let posts = Bundle(for: type(of: self)).decode([Post].self, from: "Posts.json")
        return Future<[Post], Error> { promise in
            guard let post = posts?[page] else {
                promise(.failure(APIError.invalidData))
                return
            }
            promise(.success([post]))
        }.eraseToAnyPublisher()
    }
    
    func getComments(postId: Int, page: Int, limit: Int) -> AnyPublisher<[Comment], Error> {
        let comments = Bundle(for: type(of: self)).decode([Comment].self, from: "Comments.json")
        return Future<[Comment], Error> { promise in
            guard let comment = comments?[page] else {
                promise(.failure(APIError.invalidData))
                return
            }
            promise(.success([comment]))
        }.eraseToAnyPublisher()
    }
}
