import Foundation
@testable import Blogger

struct AuthorsPresenterDependenciesMock: AuthorsPresenterDependenciesType {
    var interactor: AuthorsInteractorType = AuthorsInteractorMock()
}
