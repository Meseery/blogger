import Foundation
@testable import Blogger

struct AuthorsInteractorDependenciesMock: AuthorsInteractorDependenciesType {
    var remoteDataProvider: DataServiceType = DataServiceMock()
}
