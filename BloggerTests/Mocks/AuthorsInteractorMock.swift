import Foundation
import Combine
@testable import Blogger

class AuthorsInteractorMock: AuthorsInteractorType {
    var dependencies: AuthorsInteractorDependenciesType = AuthorsInteractorDependenciesMock()
    
    var allAuthorsLoaded: Bool = false
    
    func getAuthors() -> AnyPublisher<[Author], Error> {
        dependencies.remoteDataProvider.getAuthors(page: 1, limit: 10)
    }
    
    func getNextAuthors() -> AnyPublisher<[Author], Error> {
        dependencies.remoteDataProvider.getAuthors(page: 2, limit: 10)
    }
}
