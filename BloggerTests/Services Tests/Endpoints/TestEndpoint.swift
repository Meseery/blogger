import Foundation
@testable import Blogger
struct TestEndpoint: APIEndpointType {
    var baseURL: String { return "jsonplaceholder.typicode.com" }
    
    var path: String? { return "/todos/1"}
    
    var method: HTTPMethod { return .get}
    
    var parameters: Parameters? { return nil }
    
    var headers: HTTPHeaders? { return nil }
    
    var scheme: String? { return nil }
}

struct Todo: Decodable {
    let userId: Int, id: Int, title: String, completed: Bool
}
