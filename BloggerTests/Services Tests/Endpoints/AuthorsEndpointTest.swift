import Foundation
import XCTest
import Combine
@testable import Blogger

class AuthorsEndpointTest: XCTestCase {
    var cancellable: AnyCancellable?
    
    func test_fetchAuthors() {
        let authorsEndpoint = AuthorsEndpoint.authors(1, limit: 20)
        let exp = self.expectation(description: "Authors Endpoint Test")
        cancellable = authorsEndpoint
            .execute(responseType: [Author].self)
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    exp.fulfill()
                }
            }) { (authors) in
                XCTAssertNotNil(authors)
                XCTAssert(authors.count == 20)
                exp.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_fetchPosts() {
        let postsEndpoint = AuthorsEndpoint.posts(authorId: "1", page: 1, limit: 10)
        let exp = self.expectation(description: "Posts Endpoint Test")
        cancellable = postsEndpoint
            .execute(responseType: [Post].self)
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    exp.fulfill()
                }
            }) { (posts) in
                XCTAssertNotNil(posts)
                XCTAssert(posts.count == 10)
                exp.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_fetchComments() {
        let commentsEndpoint = AuthorsEndpoint.comments(postId: "1", page: 1, limit: 30)
        let exp = self.expectation(description: "Comments Endpoint Test")
        cancellable = commentsEndpoint
            .execute(responseType: [Comment].self)
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    exp.fulfill()
                }
            }) { (comments) in
                XCTAssertNotNil(comments)
                XCTAssert(comments.count == 30)
                exp.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    override func tearDown() {
        cancellable?.cancel()
    }
}
