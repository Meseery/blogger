import Foundation
import XCTest
import Combine
@testable import Blogger

class RemoteDataProviderTests: XCTestCase {
    var testCancellable: AnyCancellable?
    var sut: DataServiceType?
    
    override func setUp() {
        sut = RemoteDataService()
    }

    func test_getAuthors() {
        let exp = self.expectation(description: "Getting Authors")
        testCancellable = sut?
            .getAuthors(page: 1, limit: 20)
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    exp.fulfill()
                }
            }) { (authors) in
                XCTAssertNotNil(authors)
                XCTAssert(authors.count == 20)
                exp.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_getPosts() {
        let exp = self.expectation(description: "Getting Posts")
        testCancellable = sut?
            .getPosts(authorId: 1, page: 1, limit: 20)
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    exp.fulfill()
                }
            }) { (posts) in
                XCTAssertNotNil(posts)
                XCTAssert(posts.count == 20)
                exp.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }

    func test_getComments() {
        let exp = self.expectation(description: "Getting Comments")
        testCancellable = sut?
            .getComments(postId: 1, page: 1, limit: 10)
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    exp.fulfill()
                }
            }) { (comments) in
                XCTAssertNotNil(comments)
                XCTAssert(comments.count == 10)
                exp.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    override func tearDown() {
        testCancellable?.cancel()
        sut = nil
    }
}
