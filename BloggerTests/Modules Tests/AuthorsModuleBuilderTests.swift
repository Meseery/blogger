import Foundation
import XCTest
@testable import Blogger

class AuthorsModuleBuilderTests: XCTestCase {
    func test_ModuleBuilder() {
        let presenter = AuthorsModuleBuilder.build()
        XCTAssertNotNil(presenter)
        XCTAssertNotNil(presenter.dependencies)
        XCTAssertNotNil(presenter.dependencies.interactor)
        XCTAssertNotNil(presenter.dependencies.interactor.dependencies)
    }
}
