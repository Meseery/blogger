import Foundation
import XCTest
import Combine
@testable import Blogger

class AuthorsInteractorTests: XCTestCase {
    let sut = AuthorsInteractor(dependencies:AuthorsInteractorDependenciesMock())
    var testCancellable: AnyCancellable?
    
    func test_interactor_get_Authors() {
        let exp = self.expectation(description: "Fetch First Page of Authors")
        testCancellable = sut
            .getAuthors()
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    exp.fulfill()
                }
            }) { (authors) in
                XCTAssertNotNil(authors)
                exp.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_interactor_get_Next_Authors()  {
        let exp = self.expectation(description: "Fetch next Page of Authors")
        testCancellable = sut
            .getNextAuthors()
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    exp.fulfill()
                }
            }) { (authors) in
                XCTAssertNotNil(authors)
                exp.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func test_interactor_get_Next_Authors_Error()  {
        let exp = self.expectation(description: "No authors found")
        sut.allAuthorsLoaded = true
        testCancellable = sut
            .getNextAuthors()
            .sink(receiveCompletion: { (completion) in
                if case let .failure(error) = completion {
                    XCTAssertNotNil(error)
                    guard case AuthorsInteractorError.allAuthorsLoaded = error else { XCTFail("Wrong Error Type"); return}
                    exp.fulfill()
                }
            }) { (authors) in
                XCTAssertNotNil(authors)
                exp.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
}
