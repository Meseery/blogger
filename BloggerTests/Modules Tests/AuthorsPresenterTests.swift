import Foundation
import XCTest
import Combine
@testable import Blogger

class AuthorsPresenterTests: XCTestCase {
    let sut = AuthorsPresenter(dependencies:AuthorsPresenterDependenciesMock())
    
    func test_presenter_Receive_OnAppears_Event() {
        sut.didReceiveEvent(.viewAppears)
        let exp = self.expectation(description: "Expect receive authors")
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            XCTAssertFalse(self.sut.listService.items.isEmpty)
            XCTAssertTrue(self.sut.listService.listState == .items)
            exp.fulfill()
        }
        waitForExpectations(timeout: 15, handler: nil)
    }
    
    func test_presenter_Receive_OnDisppears_Event() {
        sut.didReceiveEvent(.viewDisappears)
        let exp = self.expectation(description: "Expect receive authors")
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            XCTAssertTrue(self.sut.listService.items.isEmpty)
            XCTAssertTrue(self.sut.listService.listState == .items)
            exp.fulfill()
        }
        waitForExpectations(timeout: 15, handler: nil)
    }
    
    func test_presenter_Trigger_Action_Retry_Event() {
        sut.didTriggerAction(.retry)
        let exp = self.expectation(description: "Expect receive authors")
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
            XCTAssertFalse(self.sut.listService.items.isEmpty)
            XCTAssertTrue(self.sut.listService.listState == .items)
            exp.fulfill()
        }
        waitForExpectations(timeout: 15, handler: nil)
    }
}
