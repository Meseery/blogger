import XCTest
@testable import Blogger

class AuthorEntityTest: XCTestCase {
    func test_AuthorEntityParsing() {
        let authors = Bundle(for: type(of: self)).decode([Author].self, from: "Authors.json")
        XCTAssertNotNil(authors)
        XCTAssert(authors?.count == 3)
        XCTAssertTrue((authors?.contains(where: {$0.name == "Clinton Pagac"}) ?? false))
    }
    
    func test_AuthorEntityInitialization() {
        let author = Author(id: 0, name: "Paulo Cohello", userName: "@cohello", email: "paulo@gmail.com", avatarUrl: "")
        XCTAssert(author.id == 0)
        XCTAssert(author.name == "Paulo Cohello")
        XCTAssert(author.userName == "@cohello")
        XCTAssert(author.email == "paulo@gmail.com")
        XCTAssert(author.avatarUrl.isEmpty)
    }
}
