import XCTest
@testable import Blogger

class CommentEntityTest: XCTestCase {
    func test_CommentEntityParsing()  {
        let comments = Bundle(for: type(of: self)).decode([Comment].self, from: "Comments.json")
        XCTAssertNotNil(comments)
        XCTAssert(comments?.count == 3)
        XCTAssert(comments?.contains(where: {$0.date == "2017-02-20T02:37:31.883Z"}) ?? false)
    }
    
    func test_CommentEntityInitialization() {
        let comment = Comment(id: 0, date: "2019-10-10", body: "This's Awesome!", userName: "@mohamed", email: "", avatarUrl: "", postId: 4)
        XCTAssert(comment.id == 0)
        XCTAssert(comment.date == "2019-10-10")
        XCTAssert(comment.body == "This's Awesome!")
        XCTAssert(comment.userName == "@mohamed")
        XCTAssert(comment.email.isEmpty)
        XCTAssert(comment.avatarUrl.isEmpty)
        XCTAssert(comment.postId == 4)
    }
}
