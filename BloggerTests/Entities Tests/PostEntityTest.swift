import XCTest
@testable import Blogger

class PostEntityTest: XCTestCase {
    func test_PostEntityParsing()  {
        let posts = Bundle(for: type(of: self)).decode([Post].self, from: "Posts.json")
        XCTAssertNotNil(posts)
        XCTAssert(posts?.count == 3)
        XCTAssert(posts?.contains(where: {$0.id == 1}) ?? false)
    }
    
    func test_PostEntityInitialization() {
        let post = Post(id: 0, date: "2019-10-10", title: "Some Title", body: "", imageUrl: "", authorId: 1)
        XCTAssert(post.id == 0)
        XCTAssert(post.date == "2019-10-10")
        XCTAssert(post.title == "Some Title")
        XCTAssert(post.body.isEmpty)
        XCTAssert(post.imageUrl.isEmpty)
        XCTAssert(post.authorId == 1)
    }
}
